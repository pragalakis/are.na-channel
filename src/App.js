import React, { useState, useEffect } from 'react';
import Header from './components/Header.js';
import Images from './components/Images.js';
import Pagination from './components/Pagination.js';
import Logo from './components/Logo.js';
import './App.css';

// change this for different channel
const api = 'https://api.are.na/v2/channels/260013';

export default function App() {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [pagination, setPagination] = useState(1);

  useEffect(
    () => {
      setIsLoading(true);
      fetch(`${api}?page=${pagination}`)
        .then(function(res) {
          return res.json();
        })
        .then(function(json) {
          document.title = `${json.title}`;
          setData(json);
          setIsLoading(false);
          return json;
        });
    },
    [pagination]
  );
  return (
    <div className="app">
      {isLoading ? (
        <div className="loading">
          <Logo animate={true} />
        </div>
      ) : (
        <React.Fragment>
          <Header title={data.title} slug={data.slug} />
          <Pagination
            len={data.length}
            per={data.per}
            pagination={pagination}
            setPagination={setPagination}
          />
          <Images contents={data.contents} />
        </React.Fragment>
      )}
    </div>
  );
}
