import React from 'react';
import Image from './Image.js';
import Channel from './channel.js';

export default props => {
  return (
    <div className="grid">
      <div>
        {props.contents.map(item => (
          <div key={item.id}>
            {item.class === 'Image' || item.class === 'Link' ? (
              <Image
                id={item.id}
                srcLink={item.image.original.url}
                imgLink={item.image.square.url}
              />
            ) : item.class === 'Channel' ? (
              <Channel id={item.id} title={item.title} slug={item.slug} />
            ) : (
              ''
            )}
          </div>
        ))}
      </div>
    </div>
  );
};
