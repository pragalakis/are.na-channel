import React from 'react';

export default props => {
  return (
    <div key={props.id} className="image-wrapper">
      <a href={props.srcLink} rel="noopener noreferrer" target="_blank">
        <span style={{ backgroundImage: `url(${props.imgLink})` }} />
      </a>
    </div>
  );
};
