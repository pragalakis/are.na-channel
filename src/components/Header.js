import React from 'react';

export default props => {
  return (
    <header className="header">
      <a href={`https://are.na/channel/${props.slug}`}>{props.title}</a>
    </header>
  );
};
