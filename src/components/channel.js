import React from 'react';

export default props => {
  const channel = `https://are.na/channel/${props.slug}`;
  return (
    <div key={props.id} className="channel">
      <a href={channel} rel="noopener noreferrer" target="_blank">
        <span>{props.title}</span>
      </a>
    </div>
  );
};
