import React, { useState, useEffect } from 'react';

export default props => {
  const [pages, setPages] = useState(null);

  useEffect(() => {
    setPages(Math.ceil(props.len / props.per));
  }, []);

  return (
    <div className="pagination">
      <button
        className={props.pagination > 1 ? 'visible' : 'hidden'}
        onClick={() => props.setPagination(props.pagination - 1)}
      >
        prev/-
      </button>
      <span>
        page {props.pagination} of {pages}
      </span>
      <button
        className={props.pagination < pages ? 'visible' : 'hidden'}
        style={{ marginRight: 10 }}
        onClick={() => props.setPagination(props.pagination + 1)}
      >
        -/next
      </button>
    </div>
  );
};
